# vim:syntax=yaml
mobile_broadband_repo:
    pkgrepo.managed:
        - humanname: Jessie Backports
        - name: deb http://ftp.ch.debian.org/debian jessie-backports main
        - file: /etc/apt/sources.list.d/jessie-backports.list
        - gpgcheck: True
        - gpgkey:
            - 8B48AD6246925553
            - 7638D0442B90D010

mobile_broadband_pkgs:
    pkg.installed:
        - fromrepo: jessie-backports
        - reinstall: True
        - pkgs:
            - usb-modeswitch
            - usb-modeswitch-data
        - require:
            - pkgrepo: mobile_broadband_repo
