# vim:syntax=yaml
build_tools:
    pkg.installed:
        - pkgs: {{ pillar['build_tools']['pkgs'] }}
    file.directory:
        - name: {{ pillar['build_tools']['staging'] }}
        - user: root
        - group: root
        - dir_mode: 755
        - file_mode: 644
