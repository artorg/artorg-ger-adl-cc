# vim:syntax=yaml
{% set staging = pillar['build_tools']['staging'] %}

wifi_hotspot_pkgs:
    pkg.installed:
        - pkgs:
            - dnsmasq
#            - bridge-utils
            - iptables
            - iptables-persistent
            - libnl-genl-3-dev
            - libssl-dev

# Download, configure and build hostapd
"{{ staging }}":
    archive.extracted:
        - source: "salt://wifi_hotspot/hostapd-2.5.tar.gz"
        - source_hash: md5=69f9cec3f76d74f402864a43e4f8624f
        - archive_format: tar
        - user: root
        - group: root
        - if_missing: "{{ staging }}/hostapd-2.5"
"{{ staging }}/hostapd-2.5/hostapd/.config":
    file.managed:
        - source: "salt://wifi_hotspot/config_hostapd"
        - user: root
        - group: root
        - require:
            - archive: "{{ staging }}"
"salt://wifi_hotspot/build_hostapd.sh":
    cmd.script:
        - cwd: "{{ staging }}/hostapd-2.5/hostapd"
        - shell: /bin/bash
        - timeout: 300
        - runas: root
        - unless: test -x /usr/local/bin/hostapd
        - require:
            - pkg: build_tools
            - pkg: wifi_hotspot_pkgs
            - file: "{{ staging }}/hostapd-2.5/hostapd/.config"

# Enable NAT
"net.ipv4.ip_forward":
    sysctl.present:
        - value: 1

# Write the Iptables rules
iptables_masquerade:
    iptables.append:
        - table: nat
        - chain: POSTROUTING
        - out-interface: eth1
        - jump: MASQUERADE
        - require:
            - sysctl: "net.ipv4.ip_forward"
iptables_forward_to:
    iptables.append:
        - chain: FORWARD
        - in-interface: eth1
        - out-interface: wlan0
        - match: state
        - connstate: RELATED,ESTABLISHED
        - jump: ACCEPT
        - require:
            - iptables: iptables_masquerade
iptables_forward_from:
    iptables.append:
        - chain: FORWARD
        - in-interface: wlan0
        - out-interface: eth1
        - jump: ACCEPT
        - save: True
        - require:
            - iptables: iptables_forward_to
            - pkg: wifi_hotspot_pkgs

# Create the configuration files
"/etc/network/interfaces":
    file.managed:
        - source: "salt://wifi_hotspot/interfaces"
        - user: root
        - group: root
        - mode: 644
        - require:
            - pkg: wifi_hotspot_pkgs
            - iptables: iptables_forward_from
"/etc/network/interfaces.d/wifi-hotspot.conf":
    file.managed:
        - source: "salt://wifi_hotspot/wifi-hotspot.conf"
        - user: root
        - group: root
        - mode: 644
        - require:
            - file: "/etc/network/interfaces"
"/etc/dnsmasq.conf":
    file.managed:
        - source: "salt://wifi_hotspot/dnsmasq.conf"
        - user: root
        - group: root
        - mode: 644
        - require:
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
            - pkg: wifi_hotspot_pkgs
"/etc/dhcpcd.conf":
    file.managed:
        - source: "salt://wifi_hotspot/dhcpcd.conf"
        - user: root
        - group: root
        - mode: 644
        - require:
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
"/etc/hostapd":
    file.directory:
        - user: root
        - group: root
        - dir_mode: 755
        - file_mode: 644
        - require:
            - cmd: "salt://wifi_hotspot/build_hostapd.sh"
"/etc/hostapd/hostapd.conf":
    file.managed:
        - source: "salt://wifi_hotspot/hostapd.conf"
        - user: root
        - group: root
        - mode: 644
        - template: jinja
        - require:
            - file: "/etc/hostapd"
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
"/etc/default/hostapd":
    file.managed:
        - source: "salt://wifi_hotspot/hostapd"
        - user: root
        - group: root
        - mode: 644
        - require:
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
            - file: "/etc/hostapd/hostapd.conf"
"/usr/local/lib/systemd/system":
    file.directory:
        - user: root
        - group: root
        - dir_mode: 755
        - file_mode: 644
        - makedirs: True
"/usr/local/lib/systemd/system/hostapd.service":
    file.managed:
        - source: "salt://wifi_hotspot/hostapd.service"
        - user: root
        - group: root
        - mode: 644
        - require:
            - file: "/usr/local/lib/systemd/system"

# Enable the services
"dhcpcd.service":
    service.running:
        - watch:
            - file: "/etc/dhcpcd.conf"
        - require:
            - file: "/etc/dhcpcd.conf"
"networking.service":
    service.running:
        - watch:
            - file: "/etc/network/interfaces"
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
        - require:
            - file: "/etc/network/interfaces"
            - file: "/etc/network/interfaces.d/wifi-hotspot.conf"
"hostapd.service":
    service.running:
        - enable: True
        - watch:
            - file: "/etc/hostapd/hostapd.conf"
        - require:
            - file: "/usr/local/lib/systemd/system/hostapd.service"
            - file: "/etc/default/hostapd"
            - service: "dhcpcd.service"
            - service: "networking.service"
            - cmd: "salt://wifi_hotspot/build_hostapd.sh"
# The following line is a workaround for the failure to enable services on Debian
"systemctl enable hostapd":
    cmd.run:
        - unless: "systemctl is-enabled hostapd"
        - require:
            - service: "hostapd.service"
"dnsmasq.service":
    service.running:
        - enable: True
        - watch:
            - file: "/etc/dnsmasq.conf"
        - require:
            - file: "/etc/dnsmasq.conf"
            - service: "dhcpcd.service"
            - service: "networking.service"
            - pkg: wifi_hotspot_pkgs
# Reboot the minion
"shutdown -r 10":
    cmd.run:
        - require:
            - service: "hostapd.service"
            - service: "dnsmasq.service"
