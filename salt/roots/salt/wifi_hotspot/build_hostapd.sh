#! /bin/sh
#
# build_hostapd.sh
# Copyright (C) 2016 Eleanore Young <eleanore.young@artorg.unibe.ch>
#
# Distributed under terms of the MIT license.
#
cd hostapd
make
make install
