#! /bin/sh
#
# blelogger_build.sh
# Copyright (C) 2016 Eleanore Young <eleanore.young@artorg.unibe.ch>
#
# Distributed under terms of the MIT license.
#
mkdir -p build
cd build
cmake \
    -GNinja \
    -DCMAKE_INSTALL_PREFIX={{ pillar['blelogger']['install_prefix'] }} \
    -DCMAKE_BUILD_TYPE={{ pillar['blelogger']['build_type'] }} \
    -DENABLE_BLUETOOTH={{ pillar['blelogger']['enable_bluetooth'] }} \
    -DENABLE_PARSE={{ pillar['blelogger']['enable_parse'] }} \
    -DENABLE_FILE_WRITE={{ pillar['blelogger']['enable_file_write'] }} \
    -DENABLE_STRICT_ERROR={{ pillar['blelogger']['enable_strict_error'] }} \
    -DENABLE_ACTIVE_SCAN={{ pillar['blelogger']['enable_active_scan'] }} \
    -DBLELOGGER_USER={{ pillar['blelogger']['user'] }} \
    -DBLELOGGER_DEVICE_LOCATION={{ pillar['blelogger']['device_location'] }} \
    -DBLELOGGER_PARSE_APP_ID={{ pillar['blelogger']['parse_app_id'] }} \
    -DBLELOGGER_PARSE_SERVER_URL={{ pillar['blelogger']['parse_server_url'] }} \
    -DBLELOGGER_PARSE_SESSION_TOKEN={{ pillar['blelogger']['parse_session_token'] }} \
    {{ "/".join([pillar['build_tools']['staging'], pillar['blelogger']['local_repo']]) }}
ninja
ninja install
