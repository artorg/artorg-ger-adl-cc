# vim:syntax=yaml
blelogger:
  pkg.installed:
    - pkgs:
      - bluez
      - libbluetooth-dev
      - libsystemd-dev
      - pkg-config
      - libcurl4-gnutls-dev
      - uuid-dev

  user.present:
    - name: {{ pillar['blelogger']['user'] }}
    - gid_from_name: True
    - createhome: True

  git.latest:
    - name: {{ pillar['blelogger']['repository'] }}
    - rev: {{ pillar['blelogger']['active_branch'] }}
    - target: {{ "/".join([pillar['build_tools']['staging'], pillar['blelogger']['local_repo']]) }}
    - require:
      - user: blelogger
      - pkg: build_tools

  cmd.script:
    - source: salt://blelogger/build_blelogger.sh
    - cwd: {{ "/".join([pillar['build_tools']['staging'], pillar['blelogger']['local_repo']]) }}
    - shell: /bin/bash
    - timeout: 300
    - template: jinja
    - onchanges:
      - git: blelogger
    - require:
      - pkg: build_tools
      - pkg: blelogger
      - git: blelogger
      - user: blelogger

  service.running:
    - enable: True
    - watch:
      - cmd: blelogger
    - require:
      - cmd: blelogger
