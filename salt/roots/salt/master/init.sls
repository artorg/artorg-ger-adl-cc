# vim:syntax=yaml
master:
    pkg.installed:
        - pkgs:
            - vim
            - zsh

    user.present:
        - name: maintainer
        - password: $1$JOjv478y$nxE76b3i7Bm.GwJYUzDYb0
        - shell: /bin/zsh
        - gid_from_name: True
        - createhome: True
        - optional_groups:
            - sudo
            - admin
            - wheel
            - staff
        - require:
            - pkg: master
