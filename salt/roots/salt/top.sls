# vim:syntax=yaml
base:
    '*':
        - common
    '^adl-(logger|gateway).+':
        - match: pcre
        - build_tools
#        - hardening
    'adl-logger*':
        - blelogger
    'adl-gateway*':
        - mobile_broadband
        - wifi_hotspot
