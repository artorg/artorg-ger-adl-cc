# vim:syntax=yaml
blelogger:
    repository: https://bitbucket.org/eleanorey/artorg-ger-adl-blelogger.git
    active_branch: develop
    local_repo: artorg-ger-adl-blelogger
    install_prefix: /usr/local
    user: blelogger
    build_type: Release
    enable_bluetooth: OFF
    enable_parse: ON
    enable_file_write: ON
    enable_strict_error: OFF
    enable_active_scan: OFF
    device_location: nowhere
    parse_app_id: F552E0A4-600A-41E1-A298-86B397EB7371
    parse_server_url: https://artorg-ger-adl-db.herokuapp.com/parse/
    parse_session_token: invalid
