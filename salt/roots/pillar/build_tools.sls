# vim:syntax=yaml
build_tools:
    pkgs:
        - build-essential
        - cmake
        - ninja-build
        - git
    staging: "/var/cache/salt/staging"
