Command and Control
===================
This repository contains all the necessary code to create a Salt master and to
provision the logging units.

Installing the Salt Master
--------------------------
On a linux computer whose ports 4505 and 4506 are publicly available (these are
ZeroMQ ports), execute the Salt bootstrap script. Here, you should install a
master and a minion, allow pip installs.

.. code-block:: shell

    $ curl -o bootstrap_salt.sh -L https://bootstrap.saltstack.com
    $ sudo sh bootstrap_salt.sh -P -M stable

Then, clone the repository and create symlinks so that the Salt master can find
the necessary files.

.. code-block:: shell

    $ git clone https://bitbucket.org/eleanorey/artorg-ger-adl-cc.git
    $ sudo ln -s ./artorg-ger-adl-cc/salt/roots/pillar /srv/pillar
    $ sudo ln -s ./artorg-ger-adl-cc/salt/roots/salt /srv/salt
    $ sudo ln -s ./artorg-ger-adl-cc/salt/adl-master /etc/salt/master
    $ sudo ln -s ./artorg-ger-adl-cc/salt/adl-master-minion /etc/salt/minion

And subsequently restart the Salt master and minion processes (here with
Systemd):

.. code-block:: shell

    $ sudo systemctl restart salt-master
    $ sudo systemctl restart salt-minion

Using the Salt Master
---------------------
To use the Salt master, it is highly recommended that you read and understand
the Saltstack documentation
(https://docs.saltstack.com/en/latest/contents.html). Generally speaking, you
can use all the normal Saltstack commands, such as accept minion keys:

.. code-block:: shell

    $ sudo salt-key -A

Or apply the states:

.. code-block:: shell

    $ sudo salt '*' state.apply

Provisioning a Salt Minion
--------------------------
Minions are basically all devices that you want to manage, so in our special
case they're data loggers.

First, take a Raspberry Pi 3, and flash the stock Raspbian Jessie Lite image to an
SD card:

.. code-block:: shell

    $ curl -o raspbian.zip -L https://downloads.raspberrypi.org/raspbian_lite_latest
    $ unzip raspbian.zip
    $ ...

Then, insert the SD card into the Raspberry Pi, connect an ethernet cable and
power it up. In order for the device to be able to connect to the Salt master
within ARTORG, you must define static IP settings in
``/etc/network/interfaces``!

Once the device has completed booting, ``ssh`` into the device as ``pi`` user or
connect a screen and a keyboard to the device. Issue the command ``sudo
raspi-config`` and extend the partition size (this is the top menu entry).
Reboot the device once finished.

Then, issue:

.. code-block:: shell

    $ sudo apt-get update
    $ sudo apt-get -y dist-upgrade
    $ sudo reboot

Once this is done, you can install the Salt minion as follows:

.. code-block:: shell

    $ curl -o bootstrap_salt.sh -L https://bootstrap.saltstack.com
    $ sudo sh bootstrap_salt.sh -P -A [IP Address of Salt master] -i [Minion ID] stable

Once this process is done, disconnect from the minion and apply the appropriate
provisioning steps on the Salt master.

Naming of Salt Minions
----------------------
For the use during the ADL monitoring study, the naming convention of Salt
minions works as follows: For each study participant with a C-number (e.g.
C001), there shall exist any number of loggers defined by their location (e.g.
kitchen-sink-above) in an apartment. ``adl-logger-[C-number]-[location]``
